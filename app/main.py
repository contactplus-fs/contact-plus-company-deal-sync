# -*- coding: utf-8 -*-
import logging
from typing import Union

from errors import ValidateException
from rest import get_entity_by_id, get_company_by_id, get_parameter, get_contacts, get_deals_by_company
from misc import (bx24, company_fields_id as cfi, deal_fields_id as dfi, sync_stage, sync_stage_decode, sync_complete,
                  deal_fields_default_values, deal_client_type_dialler, empty_cluster, deal_client_type_retail,
                  deal_new_customer, deal_new_customer_values, deal_client_type_prof_user,
                  deal_fields_prof_user_exception, company_client_type_prof_user, company_client_type_dialler,
                  company_client_type_retail, company_fields_prof_user_exception, empty_field, empty_field_dash
                  )

from services.helper import deal_fields_fill, serialize_fields_for_compare, serialize_value

from rest import get_contact_name

from services.MappingFields import MappingFields
from services.req_controller import validate_req
from services.error_alert import send_alert_message

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)


def deal_update(deal_id, fields):
    result = bx24.call(
        'crm.deal.update',
        {
            'id': deal_id,
            'fields': fields
        }
    )
    return result


def company_update(company_id, fields):
    result = bx24.call(
        'crm.company.update',
        {
            'id': company_id,
            'fields': fields
        }
    )
    return result


class Deal:
    #             Новое обращение,     Отправлено КП,    Ожидает оплату
    first_stage_statuses = ['NEW', 'PREPAYMENT_INVOICE', 'FINAL_INVOICE']
    #             Сбор информации,          Сделка успешна
    second_stage_statuses = ['PREPARATION', 'WON']

    deal_sync_completed = False
    valid = False

    def __init__(self, deal_id):
        self.deal = get_entity_by_id(deal_id)
        self.company_id = self.deal['COMPANY_ID']
        self.company_sync_field_value = get_parameter(self.deal, sync_complete)
        self.company_sync_field_status_value = get_parameter(self.deal, sync_stage) or []

        if self._validate():
            self.valid = True

            self.contacts = get_contacts(deal_id)
            self.company = get_company_by_id(self.company_id)

            self.mapping_contact = None
            logging.info(f'NEW REQ: DI {deal_id} - CI {self.company_id}')

    def _validate(self):
        if self.company_sync_field_value == '1':
            logging.info(f'deal({self.deal["ID"]}): full sync is complete')
            return False
        if str(self.deal['CATEGORY_ID']) != '0':
            logging.info(f'deal({self.deal["ID"]}): category is not 0')
            return False
        return True

    def main(self):
        if self.deal[dfi.client_type] in empty_cluster:
            self.check_deal_client_type()

        if self.deal[dfi.client_type] == deal_client_type_retail:
            self.retail_logic()
            return

        if self.company_id == '0':
            logging.info(f'deal({self.deal["ID"]}): company is undefined')
            return

        if self.deal[dfi.client_type] in empty_cluster and self.company[cfi.client_type] in empty_cluster:
            logging.info(f'deal({self.deal["ID"]}): client type is empty')
            return
        else:
            self.validate_deal_client_type()

        if sync_stage_decode.first not in self.company_sync_field_status_value:
            self.first_stage_sync()
        else:
            self.second_stage_sync()

        return 'ok'

    def retail_logic(self):
        fields = {
            dfi.work_region: '-',
            dfi.site_dialler_link: '-',
            dfi.year_on_the_market: '-',
            dfi.add_discount: 0,

            dfi.which_brand_sell: self.str_to_tuple(deal_fields_default_values[dfi.which_brand_sell]),
            dfi.price_type: deal_fields_default_values[dfi.price_type],
            dfi.dialler_category: deal_fields_default_values[dfi.dialler_category],
            dfi.equipment_sales_format: deal_fields_default_values[dfi.equipment_sales_format],

            dfi.client_type: deal_client_type_retail,

            deal_new_customer: deal_new_customer_values.no,
            sync_complete: '1'
        }
        result = deal_update(self.deal['ID'], fields)
        logging.info(f'RESULT RETAIL DEAL: {result} - {self.deal["ID"]}')

    def first_stage_sync(self):
        """
        Синхронизация полей из Компании в Сделку
        """
        if sync_stage_decode.first in self.company_sync_field_status_value:
            logging.info(f'Deal({self.deal["ID"]}) The first sync stage was completed earlier')
            return

        sync_stage_value = self.deal[sync_stage] or []
        sync_stage_value.append(sync_stage_decode.first)

        fields = deal_fields_fill(company=self.company, deal=self.deal)

        fields[sync_stage] = sync_stage_value
        fields[sync_complete] = self.check_sync_to_complete()
        new_sync_complete, not_filled_fields = self.company_filled_all_fields()
        if new_sync_complete == '1':
            fields[sync_complete] = new_sync_complete
            sync_stage_value.append(sync_stage_decode.second)
            fields[sync_stage] = sync_stage_value

        if fields[dfi.client_type] in empty_cluster and self.deal[dfi.client_type] not in empty_cluster:
            del fields[dfi.client_type]
        if self.deal[dfi.client_type] == deal_client_type_prof_user:
            fields = self.professional_user_logic(fields)
        if self.deal[dfi.client_type] == deal_client_type_dialler:
            fields = self.dialler_logic(fields)

        fields = self.validate_deals_fields(fields)

        print(f'Deal (with ID {self.deal["ID"]}) fields: {fields}')
        result = deal_update(self.deal['ID'], fields)
        logging.info(f'RESULT DEAL: {result} - {self.deal["ID"]}')

    def second_stage_sync(self):
        """
        Синхронизация полей из Сделки в Компанию
        """
        if sync_stage_decode.second in self.company_sync_field_status_value:
            logging.info(f'Deal({self.deal["ID"]}) The second sync stage was completed earlier')
            return
        all_fields_filled_in_company, not_filled_fields = self.company_filled_all_fields()

        if all_fields_filled_in_company == '1':
            logging.info(f'In Company({self.company_id}) All fields filled already')
            sync_stage_value = self.deal[sync_stage] or []
            sync_stage_value.append(sync_stage_decode.second)
            result_deal = deal_update(self.deal['ID'], {
                sync_stage: sync_stage_value,
                sync_complete: self.check_sync_to_complete()
            })
            return

        mapping = MappingFields('deal', self.deal, self.deal[dfi.client_type])

        fields = {
            cfi.work_region: self.get_str_value(self.deal, dfi.work_region),
            cfi.site_dialler_link: self.get_str_value(self.deal, dfi.site_dialler_link),
            cfi.year_on_the_market: self.get_str_value(self.deal, dfi.year_on_the_market, ''),
            cfi.add_discount: self.get_int_value(self.deal, dfi.add_discount),

            cfi.which_brand_sell: self.str_to_tuple(mapping.map_filed(cfi.which_brand_sell, dfi.which_brand_sell)),
            cfi.price_type: mapping.map_filed(cfi.price_type, dfi.price_type),
            cfi.dialler_category: mapping.map_filed(cfi.dialler_category, dfi.dialler_category),
            cfi.equipment_sales_format: mapping.map_filed(cfi.equipment_sales_format, dfi.equipment_sales_format),
            cfi.client_type: mapping.map_filed(cfi.client_type, dfi.client_type),
        }
        fields = self.prepare_company_fields_to_update(fields, not_filled_fields)

        if self.if_need_update_company(fields):
            result = company_update(self.company_id, fields)
            print(f'From Deal to Company. Company (with ID {self.company_id}) fields: {fields}')
        else:
            result = False
            logging.info(f'Info in Company: {self.company_id} is equal to Deal (ID {self.deal["ID"]}).')

        if self.deal_filled_all_fields():
            sync_stage_value = self.deal[sync_stage] or []
            sync_stage_value.append(sync_stage_decode.second)
            result_deal = deal_update(self.deal['ID'], {
                sync_stage: sync_stage_value,
                sync_complete: self.check_sync_to_complete()
            })
            logging.info(f'RESULT COMPANY: {result} \n RESULT - DEAL : {result_deal} - {self.deal["ID"]}')
        else:
            logging.info(f'Not all required fields are filled in DEAL {self.deal["ID"]}. Second stage not pass.')

    def get_str_value(self, entity: dict, field: str, default='-') -> str:
        if self.deal[dfi.client_type] == deal_client_type_dialler:
            return entity[field]
        if entity[field] in empty_cluster:
            return default
        return entity[field]

    def get_int_value(self, entity: dict, field: str) -> Union[int, str]:
        if entity[field] in empty_cluster:
            if self.deal[dfi.client_type] == deal_client_type_dialler:
                return ''
            return 0
        if entity[field].isnumeric():
            return int(entity[field])
        return entity[field]

    def deal_filled_all_fields(self) -> bool:
        for field in dfi.values():
            if self.deal[dfi.client_type] == deal_client_type_prof_user:
                if field in deal_fields_prof_user_exception:
                    continue

            if field in self.deal:
                if self.deal[field] in empty_cluster:
                    return False

        return True

    def company_filled_all_fields(self) -> list:
        """
        Проверяет все ли обязательные поля в Компании заполнены, и заполняет список не заполненных полей.
        """
        not_filled_fields = []
        all_fields_filled = '1'
        for field in cfi.values():

            if self.deal[dfi.client_type] == deal_client_type_prof_user:
                if field in company_fields_prof_user_exception:
                    continue

            if field in self.company:
                serialized_value = serialize_value(self.company[field])
                if serialized_value in empty_cluster:
                    not_filled_fields.append(field)
                    all_fields_filled = '0'
            else:
                not_filled_fields.append(field)
                all_fields_filled = '0'

        return [all_fields_filled, not_filled_fields]

    @staticmethod
    def prepare_company_fields_to_update(fields: dict, not_filled_fields: list) -> dict:
        """
        Готовит поля для обновления Компании из Сделки,
        убирает заполненные поля, чтобы они не перезаписывались в Компании.
        """
        fields_to_update = {}
        for key in not_filled_fields:
            if fields[key] not in empty_cluster:
                fields_to_update[key] = fields[key]

        return fields_to_update

    def new_check_sync_to_complete(self) -> str:
        for field in cfi.values():
            if self.deal[dfi.client_type] == deal_client_type_prof_user:
                if field in company_fields_prof_user_exception:
                    continue
            serialized_value = serialize_value(self.company[field])
            if serialized_value in empty_cluster:
                return '0'

        return '1'

    def check_sync_to_complete(self) -> str:
        deal_sync_stage = self.deal[sync_stage]
        if type(deal_sync_stage) == bool:
            return '1s' if deal_sync_stage else '0'
        if sync_stage_decode.first in self.deal[sync_stage] and sync_stage_decode.second in self.deal[sync_stage]:
            return '1'
        if self.deal[sync_complete] == '1':
            return '1'
        return '0'

    def check_deal_client_type(self):
        for contact in self.contacts:
            full_name = get_contact_name(contact)
            if 'лицо' in full_name.lower() and 'частное' in full_name.lower():
                self.deal[dfi.client_type] = deal_client_type_retail
                return

    def validate_deal_client_type(self):
        if self.deal[dfi.client_type] in empty_cluster and self.company[cfi.client_type] not in empty_cluster:
            self.mapping_contact = MappingFields('company', self.company, self.deal[dfi.client_type])
            self.deal[dfi.client_type] = self.mapping_contact.map_filed(cfi.client_type, dfi.client_type)

    def validate_deals_fields(self, fields: dict) -> dict:
        fields_old = fields.copy()
        for field in fields_old.keys():
            if field in self.deal and fields[field] in empty_cluster:
                del fields[field]
        return fields

    @staticmethod
    def professional_user_logic(fields):
        if fields[dfi.price_type] == deal_fields_default_values[dfi.price_type]:
            fields[dfi.price_type] = '1307'  # Розничный тип прайса
        if fields[dfi.equipment_sales_format] == deal_fields_default_values[dfi.equipment_sales_format]:
            fields[dfi.equipment_sales_format] = '475'  # Формат продажи оборудования фитнес, гостиница, спа
        for field in fields.keys():
            if field in deal_fields_prof_user_exception:
                if fields[field] == empty_field_dash:
                    fields[field] = empty_field
                if field in deal_fields_default_values and fields[field] == deal_fields_default_values[field]:
                    fields[field] = empty_field
                if fields[field] == 0:
                    fields[field] = empty_field
        return fields

    def if_need_update_company(self, fields):
        for key, value in fields.items():
            value_to_compare = serialize_value(value)
            company_value = serialize_value(self.company[key])
            if key in self.company:
                if value_to_compare in empty_cluster:
                    continue

                if company_value != value_to_compare:
                    return True
            else:
                return True

        return False

    @staticmethod
    def dialler_logic(fields):
        for field in fields.keys():
            if fields[field] == 0 \
                    or (field in deal_fields_default_values and
                        (deal_fields_default_values[field] == fields[field]
                         or deal_fields_default_values[field] in fields[field])):
                fields[field] = empty_field
            if fields[field] == empty_field_dash:
                fields[field] = empty_field
        return fields

    @staticmethod
    def str_to_tuple(arg: any) -> list:
        if type(arg) == list:
            return arg
        return [arg]


class Company:

    sync_stage_value = []

    def __init__(self, company_id):
        self.company_id = company_id
        self.company = get_company_by_id(company_id)
        self.deals = get_deals_by_company(company_id)
        self.valid = self._validate_client_type()

    def _validate_client_type(self):
        if self.company[cfi.client_type] in empty_cluster:
            logging.info(f'Client type is empty in Company (ID: {self.company_id})')
            return False
        if self.company[cfi.client_type] == company_client_type_retail:
            logging.info(f'Client type is Retail in Company (ID: {self.company_id}). Passing this company.')
            return False

        return True

    def _sync_stage_get(self):
        if self.is_all_fields_filled():
            sync_stage_value = [sync_stage_decode.first, sync_stage_decode.second]
            self.sync_stage_value = sync_stage_value

    def update_deals(self):
        self._sync_stage_get()
        for deal in self.deals:
            result = self._update_deal(deal)
            if result:
                logging.info(f'Updated deal in Company: {self.company_id}. RESULT - DEAL : {result} - {deal["ID"]}')
            else:
                logging.info(f'Info in Company: {self.company_id} - and in Deal: {deal["ID"]} - is equal. '
                             f'Passing this deal.')

    def _update_deal(self, deal):
        fields = deal_fields_fill(company=self.company, deal=deal)
        need_update = self.if_need_update(deal, fields)
        if need_update:
            if len(self.sync_stage_value) >= 2:
                fields[sync_stage] = self.sync_stage_value
                fields[sync_complete] = 1

            result = deal_update(deal['ID'], fields)
            return result

        return False

    def is_all_fields_filled(self):
        prof_user = False
        if self.company[cfi.client_type] == company_client_type_prof_user:
            prof_user = True

        for field in cfi.values():
            if prof_user:
                if field in company_fields_prof_user_exception:
                    continue

            if self.company[field] in empty_cluster:
                return False

        return True

    @staticmethod
    def if_need_update(deal, fields):
        serialized_deal = serialize_fields_for_compare(deal)
        serialized_fields = serialize_fields_for_compare(fields)

        for key, value in serialized_fields.items():
            if serialized_deal[dfi.client_type] == deal_client_type_prof_user:
                if key in deal_fields_prof_user_exception:
                    continue
            if value in empty_cluster and serialized_deal[key] in empty_cluster:
                # пустые и равны
                continue

            if key in serialized_deal:
                if serialized_deal[key] != value:
                    return True

        return False


def main(entity_id, event):
    try:
        if event == 'ONCRMDEALADD' or event == 'ONCRMDEALUPDATE':
            need_process = validate_req(f'DEAL:{entity_id}')
            if not need_process:
                return 'ok'

            sync_method = Deal(entity_id)
            if sync_method.valid:
                sync_method.main()
        elif event == 'ONCRMCOMPANYUPDATE':
            need_process = validate_req(f'COMPANY:{entity_id}')
            if not need_process:
                return 'ok'

            company = Company(entity_id)
            if company.valid:
                company.update_deals()

        return 'ok'

    except Exception as err:
        logging.info(err)
        send_alert_message(err, error_status=5)


# if __name__ == '__main__':
#     main('32671', 'ONCRMDEALUPDATE')
#     main('32673', 'ONCRMDEALUPDATE')
#     main('9003', 'ONCRMCOMPANYUPDATE')
