import logging
import urllib.parse

from misc import bx24, empty_cluster, select_deal_fields


def get_request_data(req: str) -> dict:
    req = req.split('&')
    req = [x.split('=') for x in req]
    logging.info('Parsing request...')
    return {x[0]: urllib.parse.unquote(x[1]).replace('+', ' ') for x in req}


def get_entity_by_id(pk: str, entity='deal') -> dict:
    return bx24.call(
        f'crm.{entity}.get',
        {'id': pk},
    )


def get_contacts(deal_pk: str) -> list:
    contacts = bx24.call('crm.deal.contact.items.get', {'id': deal_pk})
    contacts = [x['CONTACT_ID'] for x in contacts]
    if len(contacts) == 0:
        return []
    contacts = bx24.call('crm.contact.list', {'filter': {'ID': contacts}})
    return contacts if contacts not in empty_cluster else []


def get_company_by_id(pk: str) -> dict:
    if pk == '0':
        return {}
    return bx24.call(
        'crm.company.get',
        {'id': pk},
    )


def get_deals_by_company(company_id: str) -> list:
    if company_id == '0' or not company_id:
        return []
    return bx24.get_all(
        'crm.deal.list',
        {
            'filter': {'COMPANY_ID': company_id, 'CLOSED': 'N'},
            'select': select_deal_fields
        }
    )


def get_email(req: dict) -> str:
    try:
        return req['Email']
    except KeyError:
        return ''


def get_parameter(req: dict, parameter: str) -> str:
    if parameter in req:
        if req[parameter] not in [None, 'None', 'null', 'NaN', 'undefined']:
            return req[parameter]
    return ''


def get_contact_name(contact: dict) -> str:
    f_name = contact['NAME']
    s_name = contact['SECOND_NAME'] if contact['SECOND_NAME'] is not None else ''
    l_name = contact['LAST_NAME'] if contact['LAST_NAME'] is not None else ''
    return f'{l_name} {f_name} {s_name}'


def get_stuff_name(stuff_id: str) -> str:
    person = bx24.call(
        'user.get',
        {'ID': stuff_id}
    )
    f_name = person[0]['NAME']
    l_name = person[0]['LAST_NAME'] if person[0]['LAST_NAME'] is not None else ''
    return f'{f_name} {l_name}'


def get_fields(fields_type: str) -> dict:
    result = bx24.call(
        f"crm.{fields_type}.fields", {}
    )
    return result
