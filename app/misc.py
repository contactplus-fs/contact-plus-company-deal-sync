import os

from attrdict import AttrDict
from fast_bitrix24 import Bitrix


user_domain = os.environ.get('USER_DOMAIN')
bx24 = Bitrix(user_domain)

assigned_by_id = 9


# source = {
#     'order': 'Заказ с сайта',
#     'callback': 'Обратный звонок с сайта',
#     'newDealer': 'Новый дилер',
#     'landing': 'Лендинг',
#     'contacts': 'Контакт'
# }
# source_in_bitrix = {
#     'order': '2',
#     'callback': '3',
#     'newDealer': '4',
#     'landing': '5',
#     'contacts': '6'
# }
deal_type_in_bitrix = {
    'order': '1',
}
empty_cluster = [None, False, '', []]


def get_deal_type(request_type):
    if request_type in deal_type_in_bitrix:
        return deal_type_in_bitrix[request_type]
    return ''


def merge_dicts(*args):
    result = {}
    for dictionary in args:
        result.update(dictionary)
    return result


company_fields_id = AttrDict({
    'work_region': 'UF_CRM_1606663439706',
    'site_dialler_link': 'UF_CRM_1606663467873',
    'year_on_the_market': 'UF_CRM_1608118962',
    'add_discount': 'UF_CRM_1608382510',

    'which_brand_sell': 'UF_CRM_1625215724',
    'price_type': 'UF_CRM_1624979373',
    'dialler_category': 'UF_CRM_1608108579',
    'equipment_sales_format': 'UF_CRM_1608108743',
    'client_type': 'UF_CRM_1608108893',
})

company_fields_default_values = {
    'UF_CRM_1625215724': '2511',
    'UF_CRM_1624979373': '2513',
    'UF_CRM_1608108579': '2515',
    'UF_CRM_1608108743': '2517',
}

deal_fields_id = AttrDict({
    'work_region': 'UF_CRM_1603040137083',
    'site_dialler_link': 'UF_CRM_1603040399940',
    'year_on_the_market': 'UF_CRM_1605109105',
    'add_discount': 'UF_CRM_1605110032565',

    'which_brand_sell': 'UF_CRM_1625215409',
    'price_type': 'UF_CRM_1624979497',
    'dialler_category': 'UF_CRM_1603040730828',
    'equipment_sales_format': 'UF_CRM_1603621290348',
    'client_type': 'UF_CRM_1605088081346',
})

select_deal_fields = ['UF_CRM_1603040137083', 'UF_CRM_1603040399940', 'UF_CRM_1605109105', 'UF_CRM_1605110032565',
                      'UF_CRM_1625215409', 'UF_CRM_1624979497', 'UF_CRM_1603040730828', 'UF_CRM_1603621290348',
                      'UF_CRM_1605088081346'
                      ]

deal_fields_default_values = {
    'UF_CRM_1625215409': '2431',
    'UF_CRM_1624979497': '2427',
    'UF_CRM_1603040730828': '2435',
    'UF_CRM_1603621290348': '2439',
}

deal_new_customer = 'UF_CRM_1625223924'
# Исключения для Профессионального пользователя Регион работы, Новый клиент, Доп. скидка (%)
deal_fields_prof_user_exception = [deal_fields_id.work_region, deal_new_customer, deal_fields_id.add_discount]
company_fields_prof_user_exception = [company_fields_id.work_region, company_fields_id.add_discount]


deal_new_customer_values = AttrDict({
    'yes': '1731',
    'no': '1733'
})
deal_client_type_dialler = '407'
deal_client_type_prof_user = '409'
deal_client_type_retail = '1265'

company_client_type_dialler = '989'
company_client_type_prof_user = '991'
company_client_type_retail = '1739'

sync_complete = 'UF_CRM_1608103160'
sync_stage = 'UF_CRM_1638545594'
sync_stage_decode = AttrDict({
    'first': 2507,
    'second': 2509
})

empty_field = ''
empty_field_dash = '-'
