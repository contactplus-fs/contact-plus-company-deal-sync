# -*- coding: utf-8 -*-
import json
from bottle import post, run, route, request, default_app
from misc import (
    merge_dicts
)
from main import main
import services.req_controller as req_controller
import logging


logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.DEBUG)

req_controller.main()


@route('/api/contact_plus-company-deal-sync/v1/deal.update', method='POST')
def index():
    req = merge_dicts(dict(request.forms), dict(request.query.decode()))
    logging.info(f"REQ: {req}")
    return main(req['data[FIELDS][ID]'], req['event'])


if __name__ == '__main__':
    run(host='localhost', port=8881, debug=True, reloader=True)

app = default_app()
