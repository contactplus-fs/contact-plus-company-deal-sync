import requests
import logging
import json
from datetime import datetime
import pytz

# Настройки модуля
url_to_error_server = 'http://forsales.xyz:8828/forsales-server-alert/api/v1/post-error/'
api_token = 'a53870ab285182118ef5ff7a37f906c36940a902'
# ID проекта заданный на сервере
project_id = 'CP-CDS'
# Ссылка на документацию (не обязательно)
link_to_doc = 'https://forsalescrm.atlassian.net/wiki/spaces/CTPS/pages/98959394'
# Ссылка на git (не обязательно)
link_to_git = 'https://bitbucket.org/contactplus-fs/contact-plus-company-deal-sync/src/test_update/'
# Ссылка на разработчика (не обязательно)
link_to_portainer = 'http://forsales.xyz/'
# Статус ошибки по умолчанию
error_status_default = 3

# Константы модуля
header = {'Authorization': 'Token ' + api_token,
          'Content-Type': 'application/json'}
logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)


def send_alert_message(error_msg, error_status=error_status_default, trace=''):
    request_data = {
        "project_id": project_id,
        "error_description": error_msg,
        "date_time": str(datetime.now(tz=pytz.timezone('Asia/Yekaterinburg'))),
        "error_status": error_status,

        # не обязательные поля
        "trace": trace,
        "docs_link": link_to_doc,
        "git_link": link_to_git,
        "portainer_link": link_to_portainer
    }
    request_data_json = json.dumps(request_data)
    response = requests.post(url_to_error_server, data=request_data_json, headers=header)

    return response
