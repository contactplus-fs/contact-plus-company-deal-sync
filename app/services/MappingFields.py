from typing import Union

from errors import ValidateException
from misc import bx24, empty_cluster, deal_fields_id, company_fields_id, deal_client_type_dialler, \
    deal_fields_default_values, company_fields_default_values
import logging

logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
                    level=logging.INFO)


class MappingFields:

    def __init__(self, main_entity, entity, deal_type):
        self.main_entity = main_entity
        self.entity = entity
        self.deal_type = deal_type
        self._c_fs = {}  # company fields
        self._d_fs = {}  # deal fields
        self.c_f = ''  # company field key
        self.d_f = ''  # deal field key
        self.c_v = {}  # company values
        self.d_v = {}  # deal values
        self.mapped_v = {}  # mapped values

        self.result = []
        self._init_fields()

    def _init_fields(self) -> None:
        c_f = bx24.call('crm.company.fields', {})
        d_f = bx24.call('crm.deal.fields', {})
        self._c_fs = self._formatting_fields(c_f)
        self._d_fs = self._formatting_fields(d_f)

    def _formatting_value(self):
        c_f = self._c_fs[self.c_f]['keys']
        d_f = self._d_fs[self.d_f]['keys']
        self.c_v = {c_f[x]: x for x in c_f}
        self.d_v = {d_f[x]: x for x in d_f}

    def map_filed(self, c_f: str, d_f: str) -> list:
        self.c_f = c_f
        self.d_f = d_f
        if self.main_entity in ('D', 'deal'):
            self.deal_fields_to_company_fields()
        elif self.main_entity in ('C', 'company'):
            self.company_fields_to_deal_fields()
        else:
            logging.error('Неверно указана главная сущность')
            raise ValueError('Неверно указана главная сущность')
        self.validate_result()
        result = self.result
        self.result = 0
        return result

    def validate_result(self):
        if self.deal_type == deal_client_type_dialler:
            return
        if type(self.result) == list:
            self.validate_result_multiply()
        else:
            self.validate_result_default()

    def validate_result_multiply(self):
        if len(self.result) > 0:
            return
        if self.main_entity in ('D', 'deal'):
            if self.c_f in company_fields_default_values:
                self.result.append(company_fields_default_values[self.c_f])
        elif self.main_entity in ('C', 'company'):
            if self.d_f in deal_fields_default_values:
                self.result.append(deal_fields_default_values[self.d_f])

    def validate_result_default(self):
        if self.result not in empty_cluster:
            return
        if self.main_entity in ('D', 'deal'):
            if self.c_f in company_fields_default_values:
                self.result = company_fields_default_values[self.c_f]
        elif self.main_entity in ('C', 'company'):
            if self.d_f in deal_fields_default_values:
                self.result = deal_fields_default_values[self.d_f]

    def deal_fields_to_company_fields(self):
        self._formatting_value()
        self._entities_fields_comparator(self.d_f, self._d_fs, self.d_v, self.c_v)

    def company_fields_to_deal_fields(self):
        self._formatting_value()
        self._entities_fields_comparator(self.c_f, self._c_fs, self.c_v, self.d_v)

    def _entities_fields_comparator(self, field_key, fields, first_values, second_values):
        if field_key not in fields:
            logging.error(f'Не верный ключ поля "{field_key}"')
            return ''
        # Формируем словарь сопоставленных значений
        for f in first_values:
            if f in second_values:
                self.mapped_v[first_values[f]] = second_values[f]
        # Формируем результат в зависимости от типа поля
        if fields[field_key]['is_multiply']:
            result = []
            if self.entity[field_key] in empty_cluster:
                return result
            for entity_key in self.entity[field_key]:
                entity_key = str(entity_key)
                if entity_key in self.mapped_v:
                    result.append(self.mapped_v[entity_key])
            self.result = result
        else:
            if type(self.entity[field_key]) == list:
                raise ValidateException(f'{self.main_entity} ERROR, invalid field({field_key}) value - {self.entity[field_key]}')
            if self.entity[field_key] in self.mapped_v:
                self.result = self.mapped_v[self.entity[field_key]]

    @staticmethod
    def _formatting_fields(fields: dict) -> dict:
        fields = {
            f_key: {
                'keys': {
                    value['ID']: value['VALUE']
                    for value in fields[f_key]['items']
                },
                'is_multiply': fields[f_key]['isMultiple']
            }
            for f_key in fields if fields[f_key]['type'] == 'enumeration'
        }
        return fields
