from services.MappingFields import MappingFields
from misc import (deal_client_type_dialler, deal_fields_id as dfi, company_fields_id as cfi, empty_cluster,
                  deal_fields_prof_user_exception, company_client_type_dialler)
from typing import Union


def get_str_value(deal, entity: dict, field: str, default='-') -> str:
    if deal[dfi.client_type] == deal_client_type_dialler:
        return entity[field]
    if cfi.client_type in entity:
        if entity[cfi.client_type] == company_client_type_dialler:
            return entity[field]
    if entity[field] in empty_cluster:
        return default
    return entity[field]


def get_int_value(deal, entity: dict, field: str) -> Union[int, str]:
    if entity[field] in empty_cluster:
        if deal[dfi.client_type] == deal_client_type_dialler:
            return ''
        return 0
    if entity[field].isnumeric():
        return int(entity[field])
    return entity[field]


def str_to_tuple(arg: any) -> list:
    if type(arg) == list:
        return arg
    return [arg]


def deal_fields_fill(company, deal):
    """
    Заполнение полей из Компании в Сделку
    """

    mapping = MappingFields('company', company, deal[dfi.client_type])

    fields = {
        dfi.work_region: get_str_value(deal, company, cfi.work_region),
        dfi.site_dialler_link: get_str_value(deal, company, cfi.site_dialler_link),
        dfi.year_on_the_market: get_str_value(deal, company, cfi.year_on_the_market, ''),
        dfi.add_discount: get_int_value(deal, company, cfi.add_discount),

        dfi.which_brand_sell: str_to_tuple(mapping.map_filed(cfi.which_brand_sell, dfi.which_brand_sell)),
        dfi.price_type: mapping.map_filed(cfi.price_type, dfi.price_type),
        dfi.dialler_category: mapping.map_filed(cfi.dialler_category, dfi.dialler_category),
        dfi.equipment_sales_format: mapping.map_filed(cfi.equipment_sales_format, dfi.equipment_sales_format),
        dfi.client_type: mapping.map_filed(cfi.client_type, dfi.client_type),

    }

    return fields


def serialize_fields_for_compare(entity: dict) -> dict:
    serialized_entity = {}
    for key, value in entity.items():
        serialized_entity[key] = serialize_value(value)

    return serialized_entity


def serialize_value(value):
    if isinstance(value, list):
        serialized_list_value = []
        for list_value in value:
            if value is None:
                serialized_list_value.append(None)
            else:
                serialized_list_value.append(str(list_value))
        return serialized_list_value
    if value is None:
        return None
    return str(value)
