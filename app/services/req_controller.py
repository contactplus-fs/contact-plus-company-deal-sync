import time
from apscheduler.schedulers.background import BackgroundScheduler
import random
import logging

# settings
# timezone for scheduled job
TZ = 'Asia/Yekaterinburg'
# validate interval in seconds
validate_interval = 2
# interval for expired requests in seconds
expired_req_interval = 600
# the frequency of checking expired requests in seconds
job_interval = 600
# start and end limit of random interval in seconds
start_random_interval = 0.7
end_random_interval = 1.2
# switch logging
need_log = False
# end of settings

# request queue
req_dict = {}

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(filename)s %(levelname)s: %(message)s')


def validate_req(req_id: str) -> bool:
    interval = get_interval()
    time.sleep(interval)
    now_time = time.time()
    if req_id not in req_dict:
        # need process
        if need_log:
            logging.info(f'Request with id {req_id} not in queue')
        write_ts(req_id, now_time)
        return True

    validate_time = now_time - validate_interval
    if req_dict[req_id] < validate_time:
        # need process
        if need_log:
            logging.info(f"Request with id {req_id} it's time to process again.")
        write_ts(req_id, now_time)
        return True

    # don't need processing
    if need_log:
        logging.info(f"Request with id {req_id} already in queue")
    return False


def write_ts(req_id, now_time):
    req_dict[req_id] = now_time


def get_interval():
    return random.uniform(start_random_interval, end_random_interval)


def del_expired():
    validate_time = time.time() - expired_req_interval
    expired_reqs = []
    for key, value in req_dict.items():
        if value < validate_time:
            expired_reqs.append(key)

    for expired_req in expired_reqs:
        if need_log:
            logging.info(f'Request id {expired_req} expired')
        req_dict.pop(expired_req)


def main():
    logging.info(f'Request controller started')
    scheduler = BackgroundScheduler()
    scheduler.configure(timezone=TZ)
    scheduler.add_job(del_expired, 'interval', seconds=job_interval)
    scheduler.start()
