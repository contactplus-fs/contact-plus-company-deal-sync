# Deploy Notes

> Перед началом работы у заказчика необходимо взять данные для входящего Webhook

```env
    ENV USER_KEY=Ключ от вею хука
    ENV USER_ID=id пользователя
    ENV USER_AUTH_TOKEN_DEAL_С=верефикация входящих запросов создания(C)
    ENV USER_AUTH_TOKEN_DEAL_U=верефикация входящих запросов обновления(U)

```

###  Настройка nginx

> nginx должен содержать redirect на nginx в docker

> Выглядит это примерно так: 

```bash
    server {
        listen __PORT__;

        server_name _;

        location /api/bb-ek-site-form/v1/ {
                include proxy_params;
                proxy_pass http://127.0.0.1:8876;
        }
    }
```

> Минимально необходимые настройки глобального nginx

### Запускаем Dockerfile

> Мы должны находиться в одной папке с Dockerfile

```bash
    sudo docker-compose up --build -d
```

> Сервер запущен и готов к высоким боевым нагрузкам!

